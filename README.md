Download the zip file
Zip file contains 4 .exe files. Run these executble files as administrator mode. Detailed description mentioned as below
- Ballestra_Software_Installation.exe
This executable file install & executes java1.8 , postgres, pgadmin, Node, NPM, PNPM  

**Note:** Sometimes System reboot required when environmental variables not reflect after executable file execution.
-Ballestra_Application_Deployment.exe
This executable file executes. New Database Creation, Tables insertion and bringing up both ballestra frontend and backend application.

-CustomerCloudSyncScheduler.exe
This executable file executes Data insertion in the customer table.

- Ballestra_Application_Automation.exe
This executable file executes ballestra frontend and backend application. 
**Note** This script could execute when both ballestra frontend and backend application went down bymistakenly.

